/*
 Package color is for commands and applications that support ANSI color
 escapes or decimal values. Don't use it unless you are sure that is the
 only place your code will be used. These days ANSI color support is
 standard on all terminals (yes including Windows).

 OLD DOCUMENTATION -------
 Rather that incur even the minimal cost of running a method for every ANSI
 escape this package uses constants and assumes that when you use them you
 are reasonably confident you won't have to *not* use them.

 When the option to not use color must be considered, for example when
 piping output directly into vim editing sessions, you can use the Decolor()
 and Strip() functions to strip all colors or ANSI escapes known to this
 package.

 Three convience executable commands have been included for testing and can
 be used in shell scripts (although sourcing a native Bash equivalent is
 preferred).
 Color Format Transfersion
 -------------------------

 Types

 All of the type conversion functions convert different color types from one
 type to another. The color types that are included below are (named
 accordingly):
			- RGB
			- HSV
			- HSL
			- CYMK
			- Hex
			- Decimal
			- Ansi

 All functions are in the format: TYPE1toTYPE2.
 Examples: HSLtoRGB(), RGBtoHex(), CMYKtoRGB()

 All color types can be converted from one type to another except for Ansi.
 You can only turn other color types to Ansi. (This may be differnet soon)
*/
package color
