# ANSI Terminal Color in Golang

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/ethanbakerdev/colors)](https://goreportcard.com/report/gitlab.com/ethanbakerdev/colors) [![Coverage](https://gocover.io/_badge/gitlab.com/ethanbakerdev/colors)](https://gocover.io/gitlab.com/ethanbakerdev/colors) [![GoDoc](https://godoc.org/gitlab.com/ethanbakerdev/colors?status.svg)](https://godoc.org/gitlab.com/ethanbakerdev/colors)
